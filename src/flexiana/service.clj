(ns flexiana.service
  (:require [io.pedestal.http :as http]))

(defn- scramble-algo
  [s1 s2]
    (if (or (nil? s1) (empty? s1) (nil? s2) (empty? s2))
      {:status 500 :body "error condition"}
      {:status 200 :body (str (= (frequencies s1) (frequencies s2)))}))

(defn scramble
  [request]
  (let [{:keys [str1 str2]} (:query-params request)]
    (scramble-algo str1 str2)))

(def routes #{["/scramble" :get scramble :route-name :scramble]})

(def service {:env :prod
              ::http/routes routes
              ::http/resource-path "/public"
              ::http/type :jetty
              ::http/port 8080
              ::http/container-options {:h2c? true
                                        :h2? false
                                        :ssl? false}
              ::http/allowed-origins (constantly true)})

 
